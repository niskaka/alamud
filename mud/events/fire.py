# -*- coding: utf-8 -*-


from .event import Event3



class FireWithEvent(Event3):
    NAME = "fire-with"

    def perform(self):
        self.inform("fire-with")
        self.add_prop("fired")