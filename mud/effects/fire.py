# -*- coding: utf-8 -*-


from .effect import Effect3
from mud.events import FireWithEvent


class FireWithEffect(Effect3):
    EVENT = FireWithEvent


